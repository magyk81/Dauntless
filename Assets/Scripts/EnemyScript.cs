﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    public AudioClip hurtSound;
    public AudioClip hitSound;
    public AudioClip deathSound;

    public Material eyesMaterial;
    public GameObject locator;
    public GameObject playerPointsGUI;
    public string testName;

    private GameObject room = null;
    private LocationScript playerLocation;
    private LocationScript thisLocation;
    private Animator animator;
    private AudioSource audioSource;
    private WeaponScript weaponScript;

    public int health;
    public int toughness;
    public int damage;

    private Rigidbody rigidBody;
    private GameObject player;

    private int counter = 0;
    private int glowCounter = 0;
    public int level = 1;

    private bool hurting = false;
    private bool dying = false;

    private float smallerCollision = 0;
    private float mediumCollision = 0;
    private float largerCollision = 0;

	// Use this for initialization
	void Start ()
    {
        weaponScript = GameObject.FindGameObjectWithTag("Weapon").GetComponent<WeaponScript>();
        eyesMaterial.DisableKeyword("_EMISSION");

        /* Add variation to body sizes */
        transform.localScale *= Random.Range(0.95F, 1.05F);
        transform.localScale = new Vector3(
            transform.localScale.x,
            transform.localScale.y * Random.Range(0.95F, 1.05F),
            transform.localScale.z);

        rigidBody = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
        audioSource = GetComponent<AudioSource>();

        playerLocation = locator.GetComponent<LocationScript>();
        thisLocation = GetComponent<LocationScript>();
        animator = GetComponent<Animator>();

        foreach (CapsuleCollider collider in GetComponents<CapsuleCollider>())
        {
            if (collider.radius > largerCollision)
            {
                smallerCollision = largerCollision;
                largerCollision = collider.radius;
            }
            else smallerCollision = collider.radius;
        }
        mediumCollision = largerCollision;
        largerCollision += 0.2F;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (Input.GetKey(KeyCode.C)) Debug.Log(++level);

        transform.localPosition = new Vector3(transform.localPosition.x, 0.5F, transform.localPosition.z);

        travel();

        if (counter > 0) counter--;
        if (counter == 1)
        {
            counter = 0;
            animator.SetBool("Attack", false);
            //animator.SetBool("Scream", false);
            animator.SetBool("Hurt", false);

            hurting = false;

            foreach (CapsuleCollider collider in GetComponents<CapsuleCollider>())
            {
                if (collider.isTrigger) collider.radius = mediumCollision;
                else collider.radius = smallerCollision;
            }

            if (dying) die();
        }

        if (glowCounter > 0) glowCounter--;
        if (glowCounter == 1)
        {
            glowCounter = 0;
            eyesMaterial.DisableKeyword("_EMISSION");
        }
	}

    public void takeDamage(int damage)
    {
        if (dying) return;
        health = health - damage;
        if (health <= 0)
        {
            animator.SetBool("Dead", true);
            animator.SetBool("Attack", false);
            //animator.SetBool("Scream", false);
            animator.SetBool("Hurt", false);
            audioSource.clip = deathSound;
            audioSource.Play();
            foreach (CapsuleCollider collider in GetComponents<CapsuleCollider>())
            {
                collider.center = new Vector3(0, 2, 0);
            }
            animator.applyRootMotion = false;
            dying = true;
            counter = 500;
            playerPointsGUI.GetComponent<ScoreTracking>().addPoint();
        }
        else if (damage > toughness)
        {
            //Debug.Log("damage" + damage);
            //Debug.Log("toughness" + toughness);
            animator.SetBool("Hurt", true);
            animator.SetBool("Attack", false);
            //animator.SetBool("Scream", false);
            audioSource.clip = hurtSound;
            audioSource.Play();
            counter = 20;
        }
    }

    private void die()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == locator)
        {
            animator.SetBool("Attack", true);
            //animator.SetBool("Scream", false);
            animator.SetBool("Hurt", false);
            if (counter == 0)
            {
                foreach (CapsuleCollider collider in GetComponents<CapsuleCollider>())
                {
                    if (collider.isTrigger) collider.radius = largerCollision;
                }
                counter = 52;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == locator)
        {
            if (!hurting && counter < 30 && counter > 10)
            {
                audioSource.clip = hitSound;
                audioSource.Play();
                player.GetComponent<PlayerScript>().hurt(damage);
                hurting = true;
            }
        }
    }

    private void travel()
    {
        GameObject target = playerLocation.getRoom();

        if (thisLocation.getRoom() == playerLocation.getRoom())
        {
            target = locator;
        }

        /* For testing */
        //testName = target.name;

        Vector3 direction = directionTowards(target);

        float distance = distanceFrom(target);

        animator.SetFloat("Speed", level + 2 - (distance / 8F));

        if (direction != new Vector3(0, 0, 0))
            transform.localRotation = Quaternion.LookRotation(direction);
    }

    private Vector3 directionTowards(GameObject target)
    {
        if (target == null) target = locator;

        Vector3 difference = target.transform.position - transform.position;
        float max = Mathf.Max(Mathf.Abs(difference.x), 0, Mathf.Abs(difference.z));
        if (max != 0) difference /= max;
        return new Vector3(difference.x, 0, difference.z);
    }

    private float distanceFrom(GameObject target)
    {
        if (target == null) target = locator;

        Vector3 difference = target.transform.position - transform.position;

        /* Shine eyes if close enough */
        if (difference.magnitude < 15 && weaponScript.isCastingLight())
        {
            eyesMaterial.EnableKeyword("_EMISSION");
            glowCounter = 200;
        }

        return difference.magnitude;
    }
}
