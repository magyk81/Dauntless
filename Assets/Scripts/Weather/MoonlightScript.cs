﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonlightScript : MonoBehaviour {

    private List<GameObject> windows = new List<GameObject>();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addWindow(GameObject window)
    {
        windows.Add(window);
        //window.GetComponent<Light>().color = GetComponent<Light>().color;
    }

    public void enableLight(bool enable)
    {
        foreach (GameObject window in windows) window.SetActive(enable);
    }

    public void redden()
    {
        GetComponent<Light>().color = Color.red;
    }
}
