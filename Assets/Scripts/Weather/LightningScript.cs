﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningScript : MonoBehaviour {

    public GameObject[] lightningBolts_set1;
    public GameObject[] lightningBolts_set2;
    public GameObject[] lightningBolts_set3;
    public GameObject[] lightningBolts_set4;
    public GameObject[] lightningBolts_set5;

    public AudioClip[] audioClips;

    private List<GameObject> windows = new List<GameObject>();
    private AudioSource audioSource;

    private int counter = 0;
    private int frameIndex = 0;
    private int currentlyFlashing = -1;

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < lightningBolts_set1.Length; i++)
        {
            lightningBolts_set1[i].SetActive(false);
            lightningBolts_set2[i].SetActive(false);
            lightningBolts_set3[i].SetActive(false);
            lightningBolts_set4[i].SetActive(false);
            lightningBolts_set5[i].SetActive(false);
        }

        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (counter > 200)
        {
            if (Random.Range(0, 2) == 0) triggerLightning();
            counter = 0;
        }
        else counter++;


        if (frameIndex >= 70)
        {
            frameIndex = 0;
            unflashBolts(getLightningBoltSet());
            currentlyFlashing = -1;
        }
        else if (currentlyFlashing != -1) frameIndex++;

        if (frameIndex == 1 || frameIndex == 10 || frameIndex == 20
            || frameIndex == 30 || frameIndex == 50) flashBolts(getLightningBoltSet());
        else if (frameIndex == 5 || frameIndex == 15 || frameIndex == 25
            || frameIndex == 40 || frameIndex == 55)
            unflashBolts(getLightningBoltSet());

        /* For testing */
        //if (Input.GetKey(KeyCode.T)) triggerLightning();
    }

    public void addWindow(GameObject window)
    {
        windows.Add(window);
    }

    public void redden()
    {
        foreach (GameObject window in windows)
        {
            window.GetComponent<Light>().color = Color.red;
        }

        foreach (GameObject bolt in lightningBolts_set1)
        {
            bolt.GetComponent<Light>().color = Color.red;
            bolt.GetComponent<LineRenderer>().startColor = Color.red;
            bolt.GetComponent<LineRenderer>().endColor = Color.red;
        }
        foreach (GameObject bolt in lightningBolts_set2)
        {
            bolt.GetComponent<Light>().color = Color.red;
            bolt.GetComponent<LineRenderer>().startColor = Color.red;
            bolt.GetComponent<LineRenderer>().endColor = Color.red;
        }
        foreach (GameObject bolt in lightningBolts_set3)
        {
            bolt.GetComponent<Light>().color = Color.red;
            bolt.GetComponent<LineRenderer>().startColor = Color.red;
            bolt.GetComponent<LineRenderer>().endColor = Color.red;
        }
        foreach (GameObject bolt in lightningBolts_set4)
        {
            bolt.GetComponent<Light>().color = Color.red;
            bolt.GetComponent<LineRenderer>().startColor = Color.red;
            bolt.GetComponent<LineRenderer>().endColor = Color.red;
        }
        foreach (GameObject bolt in lightningBolts_set5)
        {
            bolt.GetComponent<Light>().color = Color.red;
            bolt.GetComponent<LineRenderer>().startColor = Color.red;
            bolt.GetComponent<LineRenderer>().endColor = Color.red;
        }
    }

    public void triggerLightning()
    {
        if (currentlyFlashing == -1) currentlyFlashing = Random.Range(1, 5);

        audioSource.clip = audioClips[Random.Range(0, audioClips.Length)];
        audioSource.Play();
    }

    private void flashBolts(GameObject[] boltSet)
    {
        if (boltSet == null) return;

        foreach (GameObject window in windows) window.SetActive(true);

        foreach (GameObject bolt in boltSet)
        {
            bolt.SetActive(true);
        }
        RenderSettings.skybox.SetFloat("_Exposure", 0.8F);
    }

    private void unflashBolts(GameObject[] boltSet)
    {
        if (boltSet == null) return;

        foreach (GameObject window in windows) window.SetActive(false);

        foreach (GameObject bolt in boltSet)
        {
            bolt.SetActive(false);
        }
        RenderSettings.skybox.SetFloat("_Exposure", 0.2F);
    }

    private GameObject[] getLightningBoltSet()
    {
        switch (currentlyFlashing)
        {
            case 1:
                return lightningBolts_set1;
            case 2:
                return lightningBolts_set2;
            case 3:
                return lightningBolts_set3;
            case 4:
                return lightningBolts_set4;
            case 5:
                return lightningBolts_set5;
            default:
                return null;
        }
    }
}
