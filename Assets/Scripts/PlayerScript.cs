﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public GameObject fpCamera;
    public GameObject sword;
    public GameObject betterSword;
    public CollectionManager stuff;

    public float maxSpeed;

    private AudioSource audioSource;

    private static int points = 0;

	void Start ()
    {
        audioSource = GetComponent<AudioSource>();
        stuff = GetComponent<CollectionManager>();
	}
	
	void FixedUpdate ()
    {
        /* Respond to key input */
        /*if (Input.GetKey(KeyCode.W)) move(true, false);
        else if (Input.GetKey(KeyCode.S)) move(false, false);
        if (Input.GetKey(KeyCode.A)) move(true, true);
        else if (Input.GetKey(KeyCode.D)) move(false, true);
        if (Input.GetKey(KeyCode.LeftArrow)) turn(true);
        else if (Input.GetKey(KeyCode.RightArrow)) turn(false);*/

        /*if (Input.GetKey(KeyCode.Space))
            sword.GetComponent<WeaponScript>().attack();*/
        
        /* Restore rotation to friendly value */
        limitEulers();

        /* Force camera to be where the player is */
        /*fpCamera.transform.position = new Vector3(
            transform.position.x,
            transform.position.y + transform.localScale.y / 2,
            transform.position.z);
        fpCamera.transform.rotation = transform.rotation;*/

        if (transform.localPosition.y < -100)
            GameObject.Find("GUI Manager").GetComponent<CanvasManage>().death();
    }

    public void hurt(int damage)
    {
        if (damage == 1) GetComponent<Health>().takeDamage(damage);
        else GetComponent<Health>().takeDamage(Mathf.Max(1, 40 - stuff.getCurrentArmor()));
    }

    public void switchWeapon()
    {
        sword.SetActive(false);
        //Destroy(sword);
        betterSword.SetActive(true);
        sword = betterSword;
    }

    private void move(bool forward, bool side)
    {
        float radians = transform.eulerAngles.y * Mathf.PI / 180F;
        int forwardDir = forward ? 1 : -1;
        float newX, newZ;

        Vector3 velocity = GetComponent<Rigidbody>().velocity;
        if (side)
        {
            newX = velocity.x - Mathf.Cos(-radians) * forwardDir;
            newZ = velocity.z - Mathf.Sin(-radians) * forwardDir;
        }
        else
        {
            newX = velocity.x - Mathf.Sin(-radians) * forwardDir;
            newZ = velocity.z + Mathf.Cos(-radians) * forwardDir;
        }

        float speed = Mathf.Sqrt(newX * newX + newZ * newZ);
        if (speed > maxSpeed)
        {
            float speedDamp = maxSpeed / speed;
            int dir = newX < 0 ? -1 : 1;
            newX = Mathf.Sqrt(newX * newX * speedDamp);
            newX *= dir;
            dir = newZ < 0 ? -1 : 1;
            newZ = Mathf.Sqrt(newZ * newZ * speedDamp);
            newZ *= dir;
        }

        GetComponent<Rigidbody>().velocity
            = new Vector3(newX, velocity.y, newZ);
    }

    private void turn(bool left)
    {
        Vector3 angles = transform.eulerAngles;
        float newY;

        if (left) newY = angles.y - 2;
        else newY = angles.y + 2;

        transform.eulerAngles = new Vector3(angles.x, newY, angles.z);
    }

    private void limitEulers()
    {
        Vector3 angles = transform.eulerAngles;
        if (angles.y >= 360) transform.eulerAngles
                = new Vector3(angles.x, angles.y - 360, angles.z);
        else if (angles.y < 0) transform.eulerAngles
                = new Vector3(angles.x, angles.y + 360, angles.z);
    }
}
