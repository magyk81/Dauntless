﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Mana : MonoBehaviour
{

    public const int maxMana = 1035;
    public int currentMana = maxMana;
    public Slider manaBar;

    public void useMana(int amount)
    {
        currentMana -= amount;
        if (currentMana <= 35)
        {
            currentMana = 35;
        }


        if (currentMana >= maxMana)
        {
            currentMana = maxMana;
        }
        else
        {
            manaBar.value = currentMana;
        }
    }

    public void restore()
    {
        currentMana = maxMana;
        manaBar.value = currentMana;
    }
}
