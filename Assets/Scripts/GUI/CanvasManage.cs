﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class CanvasManage : MonoBehaviour
{
    public GameObject messageObject;
    public GameObject mainMenu;
    public GameObject gameOverScreen;
    public GameObject player;
    public Canvas inGame;
    public Canvas pauseMenu;
    public Canvas libraryGUI; //Open with 'E'
    public GameObject playerLocator;

    public Button startButton;
    public Button endButton;
    public Button endButton2;
    public Button endButton3;
    public Button restartButton;
    public Button resumeBtn;
    public Button pauseMM;
    public Button light;
    public Button lightWounds;
    public Button smite;
    public Button haste;
    public Button libraryResume; 
    public Button purchaseSpell;
    public Sprite lightOn;
    public Sprite healOn;
    public Sprite smiteOn;
    public Sprite hasteOn;

    private Button[] spellIconList;
    private Spells[] spellInfoList;
    private Sprite[] spellImages;
    private int spellIndex;

    public Text descriptionTxt;
    public Text costTxt;
    public Text hotKeyTxt;
    public Text rankTxt;
    public Text scoreBoard;
    public Text libraryPoints;

    private RectTransform messageRectTransform;
    private LocationScript playerLocation;
    private Text messageObjectText;
    private FirstPersonController firstPersonController;
    private CollectionManager collectionManager;
    private ScoreTracking scoreTracker;

    private bool paused = false;
    private bool displayingMessage = false;
    private int messagePosY, messagePosYBottom = 75, messagePosYTop = 225;

    // Use this for initialization
    void Start()
    {
        gameOverScreen.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
        inGame.gameObject.SetActive(false);
        libraryGUI.gameObject.SetActive(false);
        startButton.onClick.AddListener(startButtonOnClick);
        endButton.onClick.AddListener(endButtonOnClick);
        restartButton.onClick.AddListener(restartButtonOnClick);
        endButton2.onClick.AddListener(endButtonOnClick);
        endButton3.onClick.AddListener(endButtonOnClick);
        pauseMM.onClick.AddListener(pauseMMOnClick);
        resumeBtn.onClick.AddListener(resumeBtnOnClick);
        libraryResume.onClick.AddListener(resumeLibraryBtnOnClick);
        purchaseSpell.onClick.AddListener(purchaseSpellBtnOnClick);
        light.onClick.AddListener(lightBtnOnClick);
        lightWounds.onClick.AddListener(lightWoundsBtnOnClick);
        smite.onClick.AddListener(smiteBtnOnClick);
        haste.onClick.AddListener(hasteBtnOnClick);

        spellIndex = 0;

        paused = true;
        player.GetComponent<FirstPersonController>().enabled = false;

        messageRectTransform = messageObject.GetComponent<RectTransform>();
        messagePosY = messagePosYBottom;


        spellIconList = new Button[4];
        spellIconList[0] = light;
        spellIconList[1] = lightWounds;
        spellIconList[2] = smite;
        spellIconList[3] = haste;

        spellImages = new Sprite[4];
        spellImages[0] = lightOn;
        spellImages[1] = healOn;
        spellImages[2] = smiteOn;
        spellImages[3] = hasteOn;

        spellInfoList = new Spells[4];
        spellInfoList[0] = new Spells(0, "Right Mouse Button", 1, "A spell that lights up an area.");
        spellInfoList[1] = new Spells(1, "Right Mouse Button", 0, "A spell that recovers HP.");
        spellInfoList[2] = new Spells(2, "Left Mouse Button", 0, "A spell that empowers attacks every 5 seconds.");
        spellInfoList[3] = new Spells(3, "Shift", 0, "A spell that makes you move more quickly. This spell does not need to be selected to be used.");

        playerLocation = playerLocator.GetComponent<LocationScript>();
        messageObjectText = messageObject.GetComponent<Text>();
        firstPersonController = player.GetComponent<FirstPersonController>();
        collectionManager = player.GetComponent<CollectionManager>();
        scoreTracker = scoreBoard.GetComponent<ScoreTracking>();
    }

    // Update is called once per frame
    void Update()
    {
        pauseOpenWatch();
        if (paused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        if (displayingMessage)
        {
            if (messagePosY > messagePosYTop)
            {
                messagePosY = messagePosYBottom;
                messageObjectText.text = "";
                displayingMessage = false;
            }
            else
            {
                messagePosY += 2;
            }
            messageRectTransform.anchoredPosition = new Vector2(3.5F, messagePosY);
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            if (!paused && playerLocation.inLibrary())
            {
                paused = true;
                firstPersonController.m_MouseLook.SetCursorLock(false);
                Cursor.lockState = CursorLockMode.None; //Still buggy
                Cursor.visible = true;
                libraryGUI.gameObject.SetActive(true);
                inGame.gameObject.SetActive(false);
                libraryPoints.GetComponent<Text>().text = scoreBoard.GetComponent<Text>().text;
            }
        }
    }

    void startButtonOnClick()
    {
        paused = false;
        mainMenu.SetActive(false);
        gameOverScreen.SetActive(false);
        inGame.gameObject.SetActive(true);
        firstPersonController.enabled = true;
        firstPersonController.GetComponent<Health>().restore();
        firstPersonController.GetComponent<Mana>().restore();
    }
    void restartButtonOnClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        startButtonOnClick();
    }

    void endButtonOnClick() // Will work ONLY as a .exe, not in Unity
    {
        Application.Quit();
    }

    void purchaseSpellBtnOnClick() 
    {
        collectionManager.buySpell(spellIndex); //spellList has the extra "-" value at 0
        scoreTracker.spendPoints(spellInfoList[spellIndex].getCost());  //need to keep track of this in CollectionManager for each spell. currently everything is cost:1
        libraryPoints.GetComponent<Text>().text = "Points: " + scoreBoard.GetComponent<ScoreTracking>().getScore();

        spellIconList[spellIndex].image.sprite = spellImages[spellIndex];
        spellInfoList[spellIndex].updateRank();

        if (collectionManager.checkSpell(spellIndex))
        {
            purchaseSpell.GetComponent<Button>().interactable = false;
            descriptionTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getDescription();
            costTxt.GetComponent<Text>().text = "Cost: " + spellInfoList[spellIndex].getCost();
            hotKeyTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getHotKey();
            rankTxt.GetComponent<Text>().text = "Rank: " + spellInfoList[spellIndex].getRank();
        }
        else if (scoreTracker.getScore() >= spellInfoList[spellIndex].getCost())
        {
            purchaseSpell.GetComponent<Button>().interactable = true;
            descriptionTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getDescription();
            costTxt.GetComponent<Text>().text = "Cost: " + spellInfoList[spellIndex].getCost();
            hotKeyTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getHotKey();
            rankTxt.GetComponent<Text>().text = "Rank: " + spellInfoList[spellIndex].getRank();
        }
        else if (scoreTracker.getScore() < spellInfoList[spellIndex].getCost())
        {
            purchaseSpell.GetComponent<Button>().interactable = false;
            descriptionTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getDescription();
            costTxt.GetComponent<Text>().text = "Cost: " + spellInfoList[spellIndex].getCost();
            hotKeyTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getHotKey();
            rankTxt.GetComponent<Text>().text = "Rank: " + spellInfoList[spellIndex].getRank();
        }
    }

    void pauseMMOnClick() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    void resumeBtnOnClick() 
    {
        paused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        inGame.gameObject.SetActive(true);
        pauseMenu.gameObject.SetActive(false);
    }
    void resumeLibraryBtnOnClick()
    {
        paused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        inGame.gameObject.SetActive(true);
        libraryGUI.gameObject.SetActive(false);

        spellIndex = 0;
        descriptionTxt.GetComponent<Text>().text = "Select a Spell to see the upgrade cost, descrition and how to use it.";
        costTxt.GetComponent<Text>().text = "";
        hotKeyTxt.GetComponent<Text>().text = "";
        rankTxt.GetComponent<Text>().text = "";
        purchaseSpell.GetComponent<Button>().interactable = false;
    }
    void lightBtnOnClick()
    {
        spellIndex = 0;
        updateDescription();
    }
    void lightWoundsBtnOnClick()
    {
        spellIndex = 1;
        updateDescription();
    }
    void smiteBtnOnClick()
    {
        spellIndex = 2;
        updateDescription();
    }
    void hasteBtnOnClick()
    {
        spellIndex = 3;
        updateDescription();
    }
    void updateDescription()
    {
        if (collectionManager.checkSpell(spellIndex))
        {
            purchaseSpell.GetComponent<Button>().interactable = false;
            descriptionTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getDescription();
            costTxt.GetComponent<Text>().text = "Cost: " + spellInfoList[spellIndex].getCost();
            hotKeyTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getHotKey();
            rankTxt.GetComponent<Text>().text = "Rank: " + spellInfoList[spellIndex].getRank();
        }
        else if (scoreTracker.getScore() >= spellInfoList[spellIndex].getCost())
        {
            purchaseSpell.GetComponent<Button>().interactable = true;
            descriptionTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getDescription();
            costTxt.GetComponent<Text>().text = "Cost: " + spellInfoList[spellIndex].getCost();
            hotKeyTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getHotKey();
            rankTxt.GetComponent<Text>().text = "Rank: " + spellInfoList[spellIndex].getRank();
        }
        else if (scoreTracker.getScore() < spellInfoList[spellIndex].getCost())
        {
            purchaseSpell.GetComponent<Button>().interactable = false;
            descriptionTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getDescription();
            costTxt.GetComponent<Text>().text = "Cost: " + spellInfoList[spellIndex].getCost();
            hotKeyTxt.GetComponent<Text>().text = spellInfoList[spellIndex].getHotKey();
            rankTxt.GetComponent<Text>().text = "Rank: " + spellInfoList[spellIndex].getRank();
        }
    }
    public void pauseOpenWatch()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (!paused)
            {
                paused = true;
                firstPersonController.m_MouseLook.SetCursorLock(false);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                inGame.gameObject.SetActive(false);
                pauseMenu.gameObject.SetActive(true);
            }
        }
    }

    public void death()
    {
        paused = true;
        firstPersonController.m_MouseLook.SetCursorLock(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        inGame.gameObject.SetActive(false);
        gameOverScreen.SetActive(true);
    }

    public void displayMessage(string message)
    {
        messageObjectText.text = message;
        displayingMessage = true;
    }
}
