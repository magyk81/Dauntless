﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTracking : MonoBehaviour {

    public Text scoreTxt;
    private int score = 0;

    // Use this for initialization
    void Start () {
        scoreTxt = gameObject.GetComponent<Text>();
        scoreTxt.text = "Points: 0";
	}
	
	// Update is called once per frame
	void Update () {
        scoreTxt.text = "Points: " + score;
    }
    public void addPoint() { score++; }
    public void spendPoints(int cost) { if (score - cost >= 0) score = score - cost; }
    public int getScore() { return score; }
}
