﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour {

	public const int maxHealth = 103;
	public int currentHealth = maxHealth;
	public Slider healthBar;

	public void takeDamage(int amount)
	{
		currentHealth -= amount;
        if (currentHealth <= 3)
        {
            currentHealth = 3;
            GameObject.Find("GUI Manager").GetComponent<CanvasManage>().death();
        }
        else
        {
            healthBar.value = currentHealth;
        }
	}

    public void restore()
    {
        currentHealth = maxHealth;
        healthBar.value = currentHealth;
    }
}
