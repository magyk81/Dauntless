﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour {

    public GameObject player;
    public GameObject weapon;

    public float speed;

    private WeaponScript weaponScript;

	// Use this for initialization
	void Start ()
    {
        weaponScript = weapon.GetComponent<WeaponScript>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        travel(!weaponScript.isCastingLight());
	}

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player.GetComponent<PlayerScript>().hurt(1);
        }
    }

    private void travel(bool forward)
    {
        Vector3 direction = directionTowards(player) * speed;
        if (!forward) direction = direction * (-2F);

        transform.localPosition = new Vector3(transform.localPosition.x + direction.x, 0, transform.localPosition.z + direction.z);

        if (direction != new Vector3(0, 0, 0))
            transform.localRotation = Quaternion.LookRotation(direction * (forward ? 1 : (-1)));
    }

    private Vector3 directionTowards(GameObject target)
    {
        //if (target == null) target = locator;

        Vector3 difference = target.transform.position - transform.position;
        float max = Mathf.Max(Mathf.Abs(difference.x), 0, Mathf.Abs(difference.z));
        if (max != 0) difference /= max;
        return new Vector3(difference.x, 0, difference.z);
    }
}
