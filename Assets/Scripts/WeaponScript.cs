﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class WeaponScript : MonoBehaviour
{
    public GameObject player;
    public AudioClip[] audioClips;
    public AudioClip hitClip;

    public float armLength;
    public int swingDegrees = 80;
    public float degreesPerFrame = 6;

    public int maxLightIntense;
    public int minLightIntense;
    public int maxLightDistance;
    public int minLightDistance;
    public int illuminationSpeed;
    
    private Light magicLight;

    private float degrees;
    private bool attacking = false;
    private bool castingLight = false;
    private bool castingCure = false;
    private bool castingSmite = false;
    private bool smiteCooling = false;
    private int smiteCooldownTimer = 0;

    private float xEuler, yEuler, xPos, yPos, zPos;

    private int lightCharge = 0;
    private float lightIntense;
    private float lightDistance;

    private Direction direction;

    public bool isCastingLight()
    {
        return castingLight;
    }

    void Start()
    {
        xEuler = transform.localEulerAngles.x;
        yEuler = transform.localEulerAngles.y;
        xPos = transform.localPosition.x;
        yPos = transform.localPosition.y + 0.2F;
        zPos = transform.localPosition.z;

        transform.localPosition = new Vector3(xPos, yPos, armLength);

        degrees = swingDegrees;
        move(swingDegrees);

        magicLight = GetComponent<Light>();
        magicLight.intensity = minLightIntense;
        magicLight.range = minLightDistance;
    }

    private void FixedUpdate()
    {
        if (attacking)
        {
            //direction = getMoveDirection();

            move(degrees);

            if (direction == Direction.LEFT || direction == Direction.DOWN)
            {
                degrees -= degreesPerFrame;
                if (degrees < 0) degrees += 360;
            }
            else if (direction == Direction.RIGHT || direction == Direction.UP)
            {
                degrees += degreesPerFrame;
                if (degrees > 360) degrees -= 360;
            }

            if (degrees > swingDegrees && degrees < 360 - swingDegrees)
            {
                attacking = false;
                direction = Direction.LEFT;
                move(swingDegrees);
                GetComponent<AudioSource>().clip = audioClips[UnityEngine.Random.Range(0, 4)];
            }
        }

        if (castingLight) lightCharge += illuminationSpeed;
        else lightCharge -= illuminationSpeed;

        if (castingCure) player.GetComponent<Health>().takeDamage(-1 * (UnityEngine.Random.Range(0, 2)));

        if(smiteCooling)
        {
            if(smiteCooldownTimer >= 300)
            {
                smiteCooldownTimer = 0;
                smiteCooling = false;
            }
            else
            {
                smiteCooldownTimer++;
            }
        }

        if (lightCharge < 0) lightCharge = 0;
        else if (lightCharge > 100) lightCharge = 100;

        magicLight.intensity = ((maxLightIntense - minLightIntense)
            * (lightCharge / 100F)) + minLightIntense;
        magicLight.range = ((maxLightDistance - minLightDistance)
            * (lightCharge / 100F)) + minLightDistance;

        if (Input.GetMouseButtonDown(0)) attack();
        if (Input.GetMouseButton(1))
        {
            castMagic(true);
        }
        else
        {
            castMagic(false);
            player.GetComponent<Mana>().useMana(-2);
        }

        bool run;
        if (!(run = !Input.GetKey(KeyCode.LeftShift))) //also possibly increase animation speed while this is active
        {
            if (player.GetComponent<CollectionManager>().checkSpell(3))
            {
                player.GetComponent<Mana>().useMana(7);
                if (player.GetComponent<Mana>().currentMana == 35)
                {
                    player.GetComponent<FirstPersonController>().m_IsWalking = !run;
                }
                else
                {
                    player.GetComponent<FirstPersonController>().m_IsWalking = run;
                }
            }
        }
        else player.GetComponent<FirstPersonController>().m_IsWalking = run;
    }

    private void attack()
    {
        if (attacking) return;

        direction = getMoveDirection();

        GetComponent<AudioSource>().Play();

        attacking = true;

        if (direction == Direction.LEFT || direction == Direction.DOWN)
            degrees = swingDegrees;
        else if (direction == Direction.RIGHT || direction == Direction.UP)
            degrees = 360 - swingDegrees;
        else
        {
            attacking = false;
        }
    }

    private void castMagic(bool mouseDown)
    {
        Console.WriteLine(player.GetComponent<CollectionManager>().getCurrentSpell() + "\n");
        if (player.GetComponent<CollectionManager>().getCurrentSpell() == 0)
        {
            if (mouseDown == true) player.GetComponent<Mana>().useMana(5);

            if (player.GetComponent<Mana>().currentMana == 35)
            {
                castingLight = false;
            }
            else
            {
                castingLight = mouseDown;
            }
        }
        else if (player.GetComponent<CollectionManager>().getCurrentSpell() == 1)
        {
            if (mouseDown == true) player.GetComponent<Mana>().useMana(20);

            if (player.GetComponent<Mana>().currentMana == 35)
            {
                castingCure = false;
            }
            else
            {
                castingCure = mouseDown;
            }
        }
    }

    private void move(float degs)
    {
        bool sideAttack = direction == Direction.LEFT || direction == Direction.RIGHT;
        if (sideAttack)
        {
            transform.localPosition = new Vector3(
                Mathf.Sin(degs * Mathf.PI / 180) * armLength,
                yPos,
                Mathf.Cos(degs * Mathf.PI / 180) * armLength);
        }
        else
        {
            transform.localPosition = new Vector3(
                xPos,
                yPos,
                Mathf.Cos(degs * Mathf.PI / 180) * armLength);
        }

        int xAttack = sideAttack ? 90 : 0;
        int yAttack = sideAttack ? 0 : 90;
        int zAttack = sideAttack ? (int) degs * -1 : 90 - (int) degs;

        transform.localEulerAngles = new Vector3(
            attacking ? xAttack : xEuler, attacking ? yAttack : yEuler, zAttack);
    }

    private Direction getMoveDirection()
    {
        float vertDir = Input.GetAxis("Mouse Y");
        float horizDir = Input.GetAxis("Mouse X");

        if (Math.Abs(vertDir) < Math.Abs(horizDir))
        {
            if (horizDir > 0) return Direction.RIGHT;
            else return Direction.LEFT;
        }
        else
        {
            if (vertDir > 0) return Direction.UP;
            else return Direction.DOWN;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (attacking && other.tag == "Enemy")
        {
            GetComponent<AudioSource>().clip = hitClip;
            GetComponent<AudioSource>().Play();

            if (player.GetComponent<CollectionManager>().getCurrentWeapon() == 0)
            {
                other.GetComponent<EnemyScript>().takeDamage(10);
            }
            else
            {
                other.GetComponent<EnemyScript>().takeDamage(30);
            }

            if (smiteCooling == false)
            {
                if (player.GetComponent<CollectionManager>().getCurrentSpell() == 2)
                {
                    other.GetComponent<EnemyScript>().takeDamage(100);
                    player.GetComponent<Mana>().useMana(400);
                    smiteCooling = true;
                    castingSmite = true;
                }
            }
        }
    }

    private enum Direction
    {
        UP, DOWN, LEFT, RIGHT
    }
}
