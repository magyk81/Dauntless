﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionManager : MonoBehaviour
{

    private bool[] spells;
    private string[] spellNames;
    private bool[] items;
    private int[] itemModifiers;
    private int currentSpell;
    private int currentWeapon;
    private int currentArmor;

    public GameObject GUI;
    public Image portrait;
    public Sprite light;
    public Sprite heal;
    public Sprite smite;

    public int spellNumber;
    public int itemNumber;

    private CanvasManage canvasManager;
    private Sprite portraitImageSprite;

    void Start()
    {
        canvasManager = GUI.GetComponent<CanvasManage>();
        portraitImageSprite = portrait.GetComponent<Image>().sprite;

        spells = new bool[spellNumber];
        spellNames = new string[spellNumber];
        items = new bool[itemNumber];
        itemModifiers = new int[itemNumber];

        spellNumber = spellNumber - 1;
        itemNumber = itemNumber - 1;
        currentArmor = 0;

        spells[0] = true;
        items[0] = true;

        currentSpell = 0; //light spell
        currentWeapon = 0;  //starting sword, 10 damage

        for (int i = 1; i < spellNumber; i++)
        {
            spells[i] = false;
            spellNames[i] = "";
        }
        for (int i = 1; i < itemNumber; i++)
        {
            items[i] = false;
        }

        spellNames[0] = "Light";
        spellNames[1] = "Cure Wounds";
        spellNames[2] = "Smite";
        spellNames[3] = "Haste";

        itemModifiers[0] = 0; //base clothes
        itemModifiers[1] = 10; //new armor
        itemModifiers[2] = 5; //bracer
        itemModifiers[3] = 5; //gauntlets
        itemModifiers[4] = 10; //shield
        itemModifiers[5] = 30; //new sword
    }

    public void changeSelectedSpellUp()
    {
        currentSpell++;
        if (currentSpell > spellNumber)
        {
            currentSpell = 0;
        }
        if (spells[currentSpell] == false)
        {
            changeSelectedSpellUp();
            return;
        }
        else if (spellNames[currentSpell] == "Haste")
        {
            changeSelectedSpellUp();
            return;
        }

        canvasManager.displayMessage(spellNames[currentSpell]);
        if (spellNames[currentSpell] == "Light")
        {
            portraitImageSprite = light;
        }
        else if (spellNames[currentSpell] == "Cure Wounds")
        {
            portraitImageSprite = heal;
        }
        else if (spellNames[currentSpell] == "Smite")
        {
            portraitImageSprite = smite;
        }
    }
    public void changeSelectedSpellDown()
    {
        currentSpell--;
        if (currentSpell < 0)
        {
            currentSpell = spellNumber;
        }
        if (spells[currentSpell] == false)
        {
            changeSelectedSpellDown();
        }
        else if (spellNames[currentSpell] == "Haste")
        {
            changeSelectedSpellDown();
        }

        canvasManager.displayMessage(spellNames[currentSpell]);
        if (spellNames[currentSpell] == "Light")
        {
            portraitImageSprite = light;
        }
        else if (spellNames[currentSpell] == "Cure Wounds")
        {
            portraitImageSprite = heal;
        }
        else if (spellNames[currentSpell] == "Smite")
        {
            portraitImageSprite = smite;
        }
    }
    public void buySpell(int index)
    {
        if (index <= spellNumber) spells[index] = true;
    }
    public int getCurrentSpell() { return currentSpell; }
    public bool checkSpell(int index) { return spells[index]; }

    public bool checkItem(int index)
    {
        if (index <= itemNumber) return items[index];
        else return false;
    }
    public void obtainItem(int index)
    {
        if (index <= itemNumber)
        {
            items[index] = true;
            if (index == 5) currentWeapon = 5;
        }

    }

    public int getCurrentWeapon() { return currentWeapon; }
    public int getCurrentArmor()
    {
        for (int i = 1; i < itemNumber; i++)
        {
            if (items[i] == true)
            {
                if (i != 5) currentArmor = currentArmor + itemModifiers[i]; //add up defense modifiers for the armor pieces
            }
        }
        return currentArmor;
    }
}
