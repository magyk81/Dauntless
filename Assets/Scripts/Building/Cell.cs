﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell
{
    public bool visited = false;
    public bool dead = false;
    public bool outskirt = false;
    public int distanceFromEntrance = 0;

    private GameObject marker = null;
    public DecorManager.RoomType roomType = DecorManager.RoomType.DINING;

    private List<Cell> neighbors = new List<Cell>();
    private List<Cell> openNeighbors = new List<Cell>();
    private List<GameObject> walls = new List<GameObject>();
    public List<GameObject> entrances = new List<GameObject>();

    public float xPos, yPos, width, height;
    public int assignedEnemies = 0;

    public Cell(float xPos, float yPos, float width, float height)
    {
        this.xPos = xPos; this.yPos = yPos;
        this.width = width; this.height = height;
    }

    public void addNeighbor(Cell neighbor)
    {
        if (dead)
        {
            neighbor.setOutskirt();
            return;
        }
        if (neighbor == null) return;
        if (neighbor == this) return;
        if (neighbor.dead) return;
        if (neighbors.Contains(neighbor)) return;

        neighbors.Add(neighbor);
    }

    public bool addWall(GameObject wall)
    {
        if (marker.GetComponent<BoxCollider>().bounds.Intersects(
            wall.GetComponent<MeshRenderer>().bounds))
        {
            walls.Add(wall);
            return true;
        }
        return false;
    }

    public void addEntrance(GameObject entrance)
    {
        entrances.Add(entrance);
    }

    public void visit(Cell cell)
    {
        if (visited || dead) return;
        visited = true;

        if (cell != this)
        {
            openNeighbors.Add(cell);
            cell.openNeighbors.Add(this);
            distanceFromEntrance = cell.distanceFromEntrance + 1;
        }

        shuffle(neighbors);
        foreach (Cell neighbor in neighbors)
        {
            neighbor.visit(this);
        }
    }

    //public GameObject[] getWalls() { return walls; }

    public void setMarker(GameObject marker, float wallHeight, float radius)
    {
        bool replacing = false;
        if (this.marker != null) replacing = true;
        if (replacing) Object.Destroy(this.marker);

        this.marker = marker;

        if (replacing)
        {
            marker.name += " - (" + xPos + ", " + yPos + ")";
            marker.transform.localPosition = new Vector3(xPos - radius, wallHeight / 2F, yPos - radius);
            marker.transform.localEulerAngles = new Vector3(0, Random.Range(0, 4) * 90, 0);
        }
        else
        {
            marker.name = "Cell Marker (" + xPos + ", " + yPos + ")";
            marker.transform.localPosition = new Vector3(xPos - radius, wallHeight / 2F, yPos - radius);
            marker.transform.localScale = new Vector3(width, wallHeight - 1, height);
        }

        marker.GetComponent<RoomScript>().setCell(this);

        if (dead) marker.SetActive(false);
    }

    public void setOutskirt()
    {
        outskirt = true;
        marker.GetComponent<RoomScript>().outskirt = true;
    }

    public void destroyMarker()
    {
        if (marker != null) Object.Destroy(marker);
    }

    public bool isOpenTo(Cell cell)
    {
        if (cell == null) return false;
        return openNeighbors.Contains(cell);
    }

    public GameObject getEntranceTo(Cell other)
    {
        foreach (GameObject entrance in other.entrances)
        {
            //Debug.Log(entrance.name);
            if (entrances.Contains(entrance)) return entrance;
        }
        return null;
    }

    public List<GameObject> getWalls()
    {
        return walls;
    }

    public GameObject getMarker() { return marker; }

    public void kill()
    {
        marker.SetActive(false);
        dead = true;
    }

    private GameObject getSharedWall(Cell cell)
    {
        foreach (GameObject wall in walls)
        {
            foreach (GameObject otherWall in cell.getWalls())
            {
                if (wall == otherWall) return wall;
            }
        }

        Debug.Log("Error in getSharedWall() from Cell.cs");
        return null;
    }

    private void shuffle(List<Cell> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = (int) Mathf.Round(Random.Range(0, n));
            Cell cell = list[k];
            list[k] = list[n];
            list[n] = cell;
        }
    }
}
