﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteriorWallScript : MonoBehaviour {

    public bool horiz;

    public int counter = 30;
    private float startBoundary;
    public bool outOfBounds = false, inBounds = false;
    private bool trimming = false;
    private bool positive;

    private Transform trans;

    // Use this for initialization
    void Start()
    {
        trans = GetComponent<Transform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (counter > 0) counter--;

        if (counter == 0)
        {
            if (!trimming)
            {
                if (outOfBounds && !inBounds) gameObject.SetActive(false);
                else if (outOfBounds && inBounds)
                {
                    trimming = true;
                    float length = trans.localScale.x / 2F;
                    if (horiz)
                    {
                        float xPos = trans.localPosition.x;
                        if (xPos > 0) positive = true;
                        else positive = false;
                        startBoundary = (positive) ? xPos - length : xPos + length;
                    }
                    else
                    {
                        float zPos = trans.localPosition.z;
                        if (zPos > 0) positive = true;
                        else positive = false;
                        startBoundary = (positive) ? zPos - length : zPos + length;
                    }
                }
                else
                {
                    done();
                }
            }
            else
            {
                if (!outOfBounds && inBounds)
                {
                    done();
                }

                outOfBounds = false;

                if (horiz)
                {
                    trans.localScale = new Vector3(trans.localScale.x - 0.3F, trans.localScale.y, trans.localScale.z);
                    float length = trans.localScale.x / 2F;
                    if (length < 0.3F) done();
                    trans.localPosition = new Vector3(startBoundary + ((positive) ? length : -length), trans.localPosition.y, trans.localPosition.z);
                }
                else
                {
                    trans.localScale = new Vector3(trans.localScale.x - 0.3F, trans.localScale.y, trans.localScale.z);
                    float length = trans.localScale.x / 2F;
                    if (length < 0.3F) done();
                    trans.localPosition = new Vector3(trans.localPosition.x, trans.localPosition.y, startBoundary + ((positive) ? length : -length));
                }
                counter = 5;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Scissor")
        {
            outOfBounds = true;
        }

        if (other.tag == "Trimmer")
        {
            inBounds = true;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Scissor")
        {
            outOfBounds = true;
        }

        if (other.tag == "Trimmer")
        {
            inBounds = true;
        }
    }

    private void done()
    {
        Destroy(GetComponent<Rigidbody>());
        Destroy(this);
        return;
    }
}
