﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntranceScript : MonoBehaviour
{
    private bool isVertical;
    private Cell cell_1, cell_2;
    private GameObject room_1, room_2;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setRooms(Cell cell1, Cell cell2)
    {
        cell_1 = cell1;
        cell_2 = cell2;

        room_1 = cell1.getMarker();
        room_2 = cell2.getMarker();
    }

    public void setOrient(bool vertical)
    {
        isVertical = vertical;
    }

    private void OnTriggerExit(Collider other)
    {
        /*string otherTag = other.gameObject.tag;
        if (otherTag == "Player")
        {
            if (isVertical)
            {
                if (collider.transform.position.x > transform.position.x)
                {
                    //collider.GetComponent<PlayerScript>().setCell(cell_2);
                }
                else
                {
                    //collider.GetComponent<PlayerScript>().setCell(cell_1);
                }
            }
            else
            {
            }
        }*/
        /*else if (otherTag == "Enemy")
        {
            other.gameObject.GetComponent<LocationScript>().determineCell(cell_1, cell_2);
        }*/
    }

    private void OnTriggerEnter(Collider collider)
    {
        
    }
}
