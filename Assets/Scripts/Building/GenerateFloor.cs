﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateFloor : MonoBehaviour
{
    public GameObject lightningBolts;
    public GameObject moonlight;
    public GameObject window;
    public GameObject whiteWindow;
    public GameObject exteriorWall;
    public GameObject hWall, vWall;
    public GameObject floor;
    public GameObject entrance;
    public GameObject cellMarker;
    public GameObject trimmer;
    public GameObject scissor;

    private float wallHeight = 0, wallThickness = 0,
        doorWidth = 2.5F, doorHeight = 5;

    private float radius;
    private float[] roomRows = null, roomColumns = null;

    private Cell[][] cells;
    private GameObject[] outerWalls = new GameObject[20];
    private GameObject[] trimmers = new GameObject[20];
    private GameObject[] scissors = new GameObject[20];
    private List<GameObject> horizWalls = null, vertWalls = null, entrances = null;
    private GameObject ceiling;

    private DecorManager decorManager;

    private void Update()
    {
        if (Input.GetKey(KeyCode.X)) resetLevel();
    }

    void Start()
    {
        decorManager = GetComponent<DecorManager>();

        if (wallHeight == 0) wallHeight = exteriorWall.transform.localScale.y;
        if (wallThickness == 0) wallThickness = exteriorWall.transform.localScale.z;

        /************ CEILING ************/
        ceiling = Instantiate(floor, transform);
        ceiling.name = "Ceiling";
        ceiling.transform.localPosition =
            new Vector3(
                floor.transform.position.x,
                floor.transform.position.y + wallHeight
                - (floor.transform.localScale.y * 2),
                floor.transform.position.z);

        radius = floor.transform.localScale.x / 2.01F;
        decorManager.setValues(radius, floor.transform.position.y
                    + floor.transform.localScale.y);

        /************ OUTER WALLS ************/
        float wallLength = (radius + exteriorWall.transform.localScale.z)
                    * Mathf.Sin(Mathf.PI / 10) * 1.02F;

        exteriorWall.SetActive(true);
        for (int i = 0; i < 20; i++)
        {
            outerWalls[i] = getOuterWall();
            outerWalls[i].transform.parent = transform;
            outerWalls[i].SetActive(true);
            outerWalls[i].name = "Outer Wall #" + (i + 1);
            float radians = (Mathf.PI / 20) + (Mathf.PI * i / 10);
            outerWalls[i].transform.localPosition =
                new Vector3(
                    Mathf.Sin(radians) * radius,
                    (exteriorWall.transform.localScale.y / 2)
                    + floor.transform.position.y
                    - floor.transform.localScale.y,
                    Mathf.Cos(radians) * radius);
            outerWalls[i].transform.eulerAngles =
                new Vector3(0, radians * 180 / Mathf.PI, 0);
            outerWalls[i].transform.localScale =
                new Vector3(
                    wallLength,
                    wallHeight,
                    wallThickness);

            /************ SET SCISSORS ************/
            trimmers[i] = Instantiate(trimmer, transform);
            trimmers[i].SetActive(true);
            trimmers[i].name = "Trimmer #" + (i + 1);
            trimmers[i].transform.localPosition = outerWalls[i].transform.localPosition;
            trimmers[i].transform.eulerAngles = outerWalls[i].transform.eulerAngles;
            trimmers[i].transform.localScale = outerWalls[i].transform.localScale;
            scissors[i] = trimmers[i].transform.GetChild(0).gameObject;
            scissors[i].transform.localPosition
                = new Vector3(0, 0, 15 + (trimmers[i].transform.localScale.z * 2));
            scissors[i].transform.localScale = new Vector3(3, 1, 30);
        }

        /************ INTERIOR WALLS METHOD CALLED HERE ************/
        resetLevel();
    }

    public void resetLevel()
    {
        exteriorWall.SetActive(true);
        entrance.SetActive(true);
        hWall.SetActive(true);
        vWall.SetActive(true);

        clearInteriorWalls();
        decorManager.clearDecor();

        setupInteriorWalls();
        setupDecor();

        hWall.SetActive(false);
        vWall.SetActive(false);
        exteriorWall.SetActive(false);
        entrance.SetActive(false);
    }

    public void setupInteriorWalls()
    {
        /****** DECIDE HOW MANY ROWS AND COLUMNS THERE WILL BE ******/
        int dims = 5;
        if (GetComponent<DecorManager>().getLevelType() == DecorManager.LevelType.BLOODMOON) dims = 7;
        else if (GetComponent<DecorManager>().getLevelType() == DecorManager.LevelType.HELLISH) dims = 9;

        roomRows = new float[dims];
        roomColumns = new float[dims];

        float roomRowLength = radius * 2 / (roomRows.Length + 1);
        for (int i = 1; i < roomRows.Length + 1; i++)
        {
            roomRows[i - 1] = i * roomRowLength;
        }
        float roomColumnLength = radius * 2 / (roomColumns.Length + 1);
        for (int i = 1; i < roomColumns.Length + 1; i++)
        {
            roomColumns[i - 1] = i * roomColumnLength;
        }

        /************ INITIALIZE CELLS  ************/
        cells = new Cell[roomColumns.Length + 1][];
        for (int i = 0; i <= roomColumns.Length; i++)
        {
            cells[i] = new Cell[roomRows.Length + 1];
            for (int j = 0; j <= roomRows.Length; j++)
            {
                float roomRowDistance = (i == roomColumns.Length)
                    ? roomColumns[i - 1] + roomColumnLength: roomColumns[i];
                float roomColumnDistance = (j == roomRows.Length)
                    ? roomRows[j - 1] + roomRowLength : roomRows[j];

                cells[i][j] = new Cell(roomRowDistance - (roomColumnLength / 2F),
                    roomColumnDistance - (roomRowLength / 2F), roomColumnLength, roomRowLength);
                cells[i][j].setMarker(Instantiate(cellMarker, transform), wallHeight, radius);
            }
        }

        trimCorneredCells(roomRows.Length, roomColumns.Length);

        /************ SET CELL NEIGHBORS ************/
        for (int i = 0; i <= roomColumns.Length; i++)
        {
            for (int j = 0; j <= roomRows.Length; j++)
            {
                if (i != 0) cells[i][j].addNeighbor(cells[i - 1][j]);
                if (j != 0) cells[i][j].addNeighbor(cells[i][j - 1]);
                if (i != roomColumns.Length) cells[i][j].addNeighbor(cells[i + 1][j]);
                if (j != roomRows.Length) cells[i][j].addNeighbor(cells[i][j + 1]);
            }
        }

        /************ CALL MAZE GENERATION ************/
        generateMaze(roomColumns.Length + 1, roomRows.Length + 1);

        entrances = new List<GameObject>();

        /****** PUT HORIZONTAL INTERIOR WALLS ******/
        horizWalls = new List<GameObject>();

        float wallLengthDefault = ((radius * 2 / (roomColumns.Length + 1)) - doorWidth) / 2;
        float wallLength = wallLengthDefault;

        for (int i = 0; i < roomRows.Length; i++)
        {
            for (int j = -1; j <= roomColumns.Length; j++)
            {
                float roomColumnPos;
                if (j == -1) roomColumnPos = roomColumns[0] - roomColumnLength;
                else if (j == roomColumns.Length)
                    roomColumnPos = roomColumns[j - 1] + roomColumnLength;
                else roomColumnPos = roomColumns[j];

                /************ PLACE THE WALL ON ONE SIDE  ************/
                GameObject horizWall = Instantiate(hWall, transform);
                horizWalls.Add(horizWall);
                horizWall.SetActive(true);
                horizWall.name = "Horizontal Wall #"
                    + (i + 1) + "." + (j + 1);

                horizWall.transform.localPosition =
                    new Vector3(
                        roomColumnPos - radius + (wallLength / 2),
                        (hWall.transform.localScale.y / 2)
                        + floor.transform.position.y
                        - floor.transform.localScale.y,
                        roomRows[i] - radius);

                horizWall.transform.eulerAngles =
                    new Vector3(0, 0, 0);

                horizWall.transform.localScale =
                    new Vector3(
                        wallLength,
                        wallHeight,
                        wallThickness);

                if (j == roomColumns.Length)
                {
                    horizWall.transform.localPosition =
                    new Vector3(
                        roomColumnPos - radius - (wallLength / 2),
                        (hWall.transform.localScale.y / 2)
                        + floor.transform.position.y
                        - floor.transform.localScale.y,
                        roomRows[i] - radius);
                    horizWall.name = "Horizontal Wall Other #"
                        + (i + 1) + "." + (j + 1);
                    /* If at the edge, only place this wall ^^^ */
                    continue;
                }

                if (j != -1)
                {
                    /************ PLACE THE WALL ON THE OTHER SIDE  ************/
                    GameObject horizWallOther = Instantiate(horizWall, transform);
                    horizWalls.Add(horizWallOther);
                    horizWallOther.transform.localPosition =
                        new Vector3(
                            roomColumnPos - radius - (wallLength / 2),
                            (hWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRows[i] - radius);
                    horizWallOther.name = "Horizontal Wall Other #"
                        + (i + 1) + "." + (j + 1);
                }

                /************ PLACE WALL OVER ENTRANCE ************/
                GameObject overWall = Instantiate(horizWall, transform);
                horizWalls.Add(overWall);
                overWall.name = "Horizontal Wall Over #"
                    + (i + 1) + "." + (j + 1);
                overWall.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1.5F, 3F);

                overWall.transform.localPosition =
                        new Vector3(
                            roomColumnPos - radius
                            + (radius / (roomColumns.Length + 1)),
                            (hWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRows[i] - radius);

                overWall.transform.localScale =
                    new Vector3(
                        doorWidth,
                        wallHeight,
                        wallThickness);

                Cell[] neighboringCells = addToCells(overWall);
                if (neighboringCells == null) continue;

                if (neighboringCells[0].isOpenTo(neighboringCells[1]))
                {
                    /************ PLACE ENTRANCE ************/
                    GameObject entClone = Instantiate(entrance, transform);
                    entrances.Add(entClone);
                    entClone.SetActive(true);
                    entClone.name = "Entrance #"
                        + (i + 1) + "." + (j + 1);

                    neighboringCells[0].addEntrance(entClone);
                    neighboringCells[1].addEntrance(entClone);
                    entClone.GetComponent<EntranceScript>().setRooms(
                        neighboringCells[0], neighboringCells[1]);

                    entClone.transform.localPosition =
                        new Vector3(
                            roomColumnPos - radius
                            + (radius / (roomColumns.Length + 1)),
                            (hWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRows[i] - radius);

                    entClone.transform.eulerAngles =
                        new Vector3(0, 0, 0);

                    entClone.transform.localScale =
                        new Vector3(
                            doorWidth,
                            wallHeight,
                            wallThickness);

                    entClone.GetComponent<EntranceScript>().setOrient(false);

                    overWall.transform.localPosition =
                        new Vector3(
                            roomColumnPos - radius
                            + (radius / (roomColumns.Length + 1)),
                            doorHeight
                            + floor.transform.position.y
                            - floor.transform.localScale.y
                            + ((wallHeight - doorHeight) / 2F),
                            roomRows[i] - radius);

                    overWall.transform.localScale =
                        new Vector3(
                            doorWidth,
                            wallHeight - doorHeight,
                            wallThickness);

                    overWall.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1.25F, 0.85F);
                    overWall.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, 0.15F);
                }
            }
        }

        /****** PUT VERTICAL INTERIOR WALLS *****/
        vertWalls = new List<GameObject>();

        wallLengthDefault = ((radius * 2 / (roomRows.Length + 1)) - doorWidth) / 2;
        wallLength = wallLengthDefault;

        for (int i = 0; i < roomColumns.Length; i++)
        {
            for (int j = -1; j <= roomRows.Length; j++)
            {
                float roomRowPos;
                if (j == -1) roomRowPos = roomRows[0] - roomRowLength;
                else if (j == roomRows.Length)
                    roomRowPos = roomRows[j - 1] + roomRowLength;
                else roomRowPos = roomRows[j];

                /************ PLACE THE WALL ON ONE SIDE  ************/
                GameObject vertWall = Instantiate(vWall, transform);
                vertWalls.Add(vertWall);
                vertWall.SetActive(true);
                vertWall.name = "Vertical Wall #"
                    + (i + 1) + "." + (j + 1);

                vertWall.transform.localPosition =
                    new Vector3(
                        roomColumns[i] - radius,
                        (vWall.transform.localScale.y / 2)
                        + floor.transform.position.y
                        - floor.transform.localScale.y,
                        roomRowPos - radius + (wallLength / 2));

                vertWall.transform.eulerAngles =
                    new Vector3(0, 90, 0);

                vertWall.transform.localScale =
                    new Vector3(
                        wallLength,
                        wallHeight,
                        wallThickness);

                if (j != -1)
                {
                    /************ PLACE THE WALL ON THE OTHER SIDE  ************/
                    GameObject vertWallOther = Instantiate(vertWall, transform);
                    vertWalls.Add(vertWallOther);
                    vertWallOther.transform.localPosition =
                        new Vector3(
                            roomColumns[i] - radius,
                            (vWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRowPos - radius - (wallLength / 2));
                    vertWallOther.name = "Vertical Wall Other #"
                        + (i + 1) + "." + (j + 1);
                }

                if (j == roomRows.Length)
                {
                    vertWall.transform.localPosition =
                        new Vector3(
                            roomColumns[i] - radius,
                            (vWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRowPos - radius - (wallLength / 2));
                    vertWall.name = "Vertical Wall Other #"
                        + (i + 1) + "." + (j + 1);
                    /* If at the edge, only place this wall ^^^ */
                    continue;
                }

                /************ PLACE WALL OVER ENTRANCE ************/
                GameObject overWall = Instantiate(vertWall, transform);
                vertWalls.Add(overWall);
                overWall.name = "Vertical Wall Over #"
                    + (i + 1) + "." + (j + 1);
                overWall.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1.5F, 3F);

                overWall.transform.localPosition =
                        new Vector3(
                            roomColumns[i] - radius,
                            (vWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRowPos - radius
                            + (radius / (roomRows.Length + 1)));

                overWall.transform.localScale =
                        new Vector3(
                            doorWidth,
                            wallHeight,
                            wallThickness);

                Cell[] neighboringCells = addToCells(overWall);
                if (neighboringCells == null) continue;

                if (neighboringCells[0].isOpenTo(neighboringCells[1]))
                {
                    /************ PLACE ENTRANCE ************/
                    GameObject entClone = Instantiate(entrance, transform);
                    entrances.Add(entClone);
                    entClone.SetActive(true);
                    entClone.name = "Entrance #"
                        + (i + 1) + "." + (j + 1);

                    entClone.transform.localPosition =
                        new Vector3(
                            roomColumns[i] - radius,
                            (vWall.transform.localScale.y / 2)
                            + floor.transform.position.y
                            - floor.transform.localScale.y,
                            roomRowPos - radius
                            + (radius / (roomRows.Length + 1)));

                    entClone.transform.eulerAngles =
                        new Vector3(0, 90, 0);

                    entClone.transform.localScale =
                        new Vector3(
                            doorWidth,
                            wallHeight,
                            wallThickness);

                    entClone.GetComponent<EntranceScript>().setOrient(true);

                    overWall.transform.localPosition =
                        new Vector3(
                            roomColumns[i] - radius,
                            doorHeight
                            + floor.transform.position.y
                            - floor.transform.localScale.y
                            + ((wallHeight - doorHeight) / 2F),
                            roomRowPos - radius
                            + (radius / (roomRows.Length + 1)));

                    overWall.transform.localScale =
                        new Vector3(
                            doorWidth,
                            wallHeight - doorHeight,
                            wallThickness);

                    overWall.GetComponent<Renderer>().material.mainTextureScale = new Vector2(1.25F, 0.85F);
                    overWall.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0, 0.15F);
                }
            }
        }
    }

    private void clearInteriorWalls()
    {
        if (roomColumns != null && roomRows != null)
        {
            for (int i = 0; i <= roomColumns.Length; i++)
            {
                for (int j = 0; j <= roomRows.Length; j++)
                {
                    cells[i][j].destroyMarker();
                    cells[i][j] = null;
                }
            }
        }

        if (horizWalls != null)
        {
            foreach (GameObject wall in horizWalls) Destroy(wall);
            horizWalls.Clear();
        }
        if (vertWalls != null)
        {
            foreach (GameObject wall in vertWalls) Destroy(wall);
            vertWalls.Clear();
        }
        if (entrances != null)
        {
            foreach (GameObject ent in entrances) Destroy(ent);
            entrances.Clear();
        }
    }

    private GameObject getOuterWall()
    {
        GameObject outerWall = new GameObject();
        GameObject leftWall = Instantiate(exteriorWall, outerWall.transform);
        leftWall.name = "Sinister";
        leftWall.transform.localPosition
            = new Vector3(0.1F - 0.5F, 0, 0);
        leftWall.transform.localScale = new Vector3(0.2F, 1, 1);
        GameObject middleWall = Instantiate(exteriorWall, outerWall.transform);
        middleWall.name = "Malcolm";
        middleWall.transform.localPosition
            = new Vector3(0.5F - 0.5F, 0, 0);
        middleWall.transform.localScale = new Vector3(0.2F, 1, 1);
        GameObject rightWall = Instantiate(exteriorWall, outerWall.transform);
        rightWall.name = "Rectal";
        rightWall.transform.localPosition
            = new Vector3(0.9F - 0.5F, 0, 0);
        rightWall.transform.localScale = new Vector3(0.2F, 1, 1);

        GameObject leftUpperWall = Instantiate(exteriorWall, outerWall.transform);
        leftUpperWall.name = "Above Left Window";
        leftUpperWall.transform.localPosition
            = new Vector3(0.3F - 0.5F, 1 / 3F, 0);
        leftUpperWall.transform.localScale = new Vector3(0.2F, 1 / 3F, 1);

        GameObject leftWindow = Instantiate(window, outerWall.transform);
        leftWindow.name = "Left Window";
        leftWindow.transform.localPosition
            = new Vector3(0.3F - 0.5F, 0, 5);
        leftWindow.SetActive(false);
        lightningBolts.GetComponent<LightningScript>().addWindow(leftWindow);

        GameObject leftWhiteWindow = Instantiate(whiteWindow, outerWall.transform);
        leftWhiteWindow.name = "Left White Window";
        leftWhiteWindow.transform.localPosition
            = new Vector3(0.3F - 0.5F, 0, 5);
        leftWhiteWindow.SetActive(true);
        moonlight.GetComponent<MoonlightScript>().addWindow(leftWhiteWindow);

        GameObject leftLowerWall = Instantiate(exteriorWall, outerWall.transform);
        leftLowerWall.name = "Below Left Window";
        leftLowerWall.transform.localPosition
            = new Vector3(0.3F - 0.5F, -1 / 3F, 0);
        leftLowerWall.transform.localScale = new Vector3(0.2F, 1 / 3F, 1);

        GameObject rightUpperWall = Instantiate(exteriorWall, outerWall.transform);
        rightUpperWall.name = "Above Right Window";
        rightUpperWall.transform.localPosition
            = new Vector3(0.7F - 0.5F, 1 / 3F, 0);
        rightUpperWall.transform.localScale = new Vector3(0.2F, 1 / 3F, 1);

        GameObject rightWindow = Instantiate(window, outerWall.transform);
        rightWindow.name = "Right Window";
        rightWindow.transform.localPosition
            = new Vector3(0.7F - 0.5F, 0, 5);
        rightWindow.SetActive(false);
        lightningBolts.GetComponent<LightningScript>().addWindow(leftWindow);

        GameObject rightWhiteWindow = Instantiate(whiteWindow, outerWall.transform);
        rightWhiteWindow.name = "Right White Window";
        rightWhiteWindow.transform.localPosition
            = new Vector3(0.7F - 0.5F, 0, 5);
        rightWhiteWindow.SetActive(true);
        moonlight.GetComponent<MoonlightScript>().addWindow(rightWhiteWindow);

        GameObject rightLowerWall = Instantiate(exteriorWall, outerWall.transform);
        rightLowerWall.name = "Below Right Window";
        rightLowerWall.transform.localPosition
            = new Vector3(0.7F - 0.5F, -1 / 3F, 0);
        rightLowerWall.transform.localScale = new Vector3(0.2F, 1 / 3F, 1);

        return outerWall;
    }

    private void trimCorneredCells(int width, int height)
    {
        cells[0][0].kill();
        cells[0][width].kill();
        cells[height][width].kill();
        cells[height][0].kill();

        if (width == 4 || height == 4) return;
        if (width < 7 && height < 7) return;

        if (width == 6 || width == 5 || height == 6 || height == 5)
        {
            if (width > height)
            {
                cells[0][1].kill();
                cells[0][width - 1].kill();
                cells[height][1].kill();
                cells[height][width - 1].kill();
            }
            else
            {
                cells[1][0].kill();
                cells[1][width].kill();
                cells[height - 1][0].kill();
                cells[height - 1][width].kill();
            }
        }
        else
        {
            cells[0][1].kill();
            cells[1][0].kill();
            cells[0][width - 1].kill();
            cells[1][width].kill();
            cells[height - 1][0].kill();
            cells[height][1].kill();
            cells[height][width - 1].kill();
            cells[height - 1][width].kill();
        }
    }

    private void generateMaze(int width, int height)
    {
        Cell startingCell = cells[width / 2][0];
        startingCell.visit(startingCell);
    }

    private Cell[] addToCells(GameObject wall)
    {
        bool firstCellFound = false;
        bool secondCellFound = false;
        Cell[] foundCells = new Cell[2];

        /************ ASSIGN WALLS TO EACH CELL ************/
        for (int i = 0; i <= roomColumns.Length; i++)
        {
            for (int j = 0; j <= roomRows.Length; j++)
            {
                bool foundOne = cells[i][j].addWall(wall);
                if (foundOne && !firstCellFound)
                {
                    foundCells[0] = cells[i][j];
                    firstCellFound = true;
                }
                else if (foundOne && !secondCellFound)
                {
                    foundCells[1] = cells[i][j];
                    secondCellFound = true;
                }
            }
        }

        if (firstCellFound && secondCellFound) return foundCells;
        else return null;
    }

    private void setupDecor()
    {
        decorManager.spawnPlayer(cells,
            roomRows.Length + 1, roomColumns.Length + 1);
        decorManager.spawnEnemies(cells,
            roomRows.Length + 1, roomColumns.Length + 1);
        decorManager.spawnDecor(cells,
            roomRows.Length + 1, roomColumns.Length + 1, wallHeight);
        decorManager.spawnWeather();
    }
}