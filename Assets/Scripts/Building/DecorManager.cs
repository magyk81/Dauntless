﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorManager : MonoBehaviour
{
    public GameObject player;
    public GameObject ladder;
    public GameObject zombie;
    public GameObject troll;
    public GameObject goblin;
    public GameObject necromancer;
    public GameObject enemyHolder;
    public GameObject cellMarker;
    public GameObject furnitureSetup;

    public GameObject[] items;
    private int itemsCollected = 0;

    public GameObject moonlight;
    public GameObject lightningBolts;

    public int startingEnemies, enemyIncPerLevel, maxEnemiesPerCell;
    public int lightningLevel, bloodmoonLevel, hellishLevel;

    private List<GameObject> decorObjects = new List<GameObject>();
    private List<GameObject> enemies = new List<GameObject>();

    private Cell closestCell = null, farthestCell = null;

    private int estStartingEnemies;
    private float radius, floorPos;
    private int decoratableCells = 0;
    public int level = 1;

    private void Start()
    {
        estStartingEnemies = startingEnemies + (level * enemyIncPerLevel);
    }

    public void spawnPlayer(Cell[][] cells, int rows, int columns)
    {
        int maxDistance = 0;
        int minDistance = 100;
        farthestCell = null;
        closestCell = null;
        decoratableCells = 0;
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                if (cells[i][j].dead || cells[i][j].outskirt) continue;
                decoratableCells++;

                int distance = cells[i][j].distanceFromEntrance;
                if (distance > maxDistance)
                {
                    maxDistance = distance;
                    farthestCell = cells[i][j];
                }
                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestCell = cells[i][j];
                }
            }
        }

        GameObject ladderClone = Instantiate(ladder, transform);
        decorObjects.Add(ladderClone);
        ladderClone.SetActive(true);
        if (getLevelType() == LevelType.HELLISH)
            ladderClone.GetComponent<ItemScript>().setVictory();
        placeDecorCenter(farthestCell, ladderClone);

        placeDecorCenter(closestCell, player);

        /* For debugging */
        //placeDecorCenter(closestCell, Instantiate(goblin, enemyHolder.transform));
    }

    public void spawnEnemies(Cell[][] cells, int rows, int columns)
    {
        int estNumCells = decoratableCells;
        int minEnemiesPerCell = 0;
        int enemiesRemaining = estStartingEnemies;
        while (estNumCells < enemiesRemaining && minEnemiesPerCell < 3)
        {
            minEnemiesPerCell++;
            enemiesRemaining -= estNumCells;
        }

        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                cells[i][j].assignedEnemies = 0;
                cells[i][j].roomType = RoomType.DINING;
            }
        }

        enemiesRemaining = estStartingEnemies;
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                if (cells[i][j].dead || cells[i][j].outskirt) continue;
                if (cells[i][j] == closestCell) continue;

                int enemiesAssigned = Random.Range(minEnemiesPerCell, Mathf.Min(level + 2, maxEnemiesPerCell + 1));
                cells[i][j].assignedEnemies = enemiesAssigned;
                enemiesRemaining -= enemiesAssigned;

                if (enemiesRemaining <= 0) break;
            }
            if (enemiesRemaining <= 0) break;
        }

        zombie.SetActive(true);
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                int assignedEnemies = cells[i][j].assignedEnemies;
                GameObject[] spawnEnemies = new GameObject[assignedEnemies];
                for (int k = 0; k < assignedEnemies; k++)
                {
                    spawnEnemies[k] = spawnEnemy();
                    enemies.Add(spawnEnemies[k]);
                }
                placeNearCenter(cells[i][j], spawnEnemies);
            }
        }
        zombie.SetActive(false);
    }

    public void spawnDecor(Cell[][] cells, int rows, int columns, float wallHeight)
    {
        assignRoomtypes(cells, rows, columns);

        furnitureSetup.SetActive(true);

        GameObject diningRoom = GameObject.Find("Dining (" + columns + " x " + rows + ")");
        diningRoom.SetActive(true);
        GameObject armoryRoom = GameObject.Find("Armory (" + columns + " x " + rows + ")");
        diningRoom.SetActive(true);
        GameObject libraryRoom = GameObject.Find("Library (" + columns + " x " + rows + ")");
        diningRoom.SetActive(true);

        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                Cell cell = cells[i][j];
                GameObject room;
                if (cell.roomType == RoomType.LIBRARY) room = libraryRoom;
                else if (cell.roomType == RoomType.ARMORY) room = armoryRoom;
                else room = diningRoom;

                if (!cell.dead && !cell.outskirt)
                    cell.setMarker(Instantiate(room, transform), wallHeight, radius);

                if (cell.roomType == RoomType.ARMORY) placeItemCenter(cell, getNextItem());

                //cell.roomType = RoomType.DINING;
            }
        }
        furnitureSetup.SetActive(false);
    }

    public void spawnWeather()
    {
        LevelType levelType = getLevelType();
        if (levelType == LevelType.NORMAL)
        {
            moonlight.SetActive(true);
            moonlight.GetComponent<MoonlightScript>().enableLight(true);
            lightningBolts.SetActive(false);
            RenderSettings.skybox.SetColor("_Tint", Color.grey);
            RenderSettings.skybox.SetFloat("_Exposure", 0.5F);
        }
        else if (levelType == LevelType.LIGHTNING)
        {
            moonlight.SetActive(false);
            moonlight.GetComponent<MoonlightScript>().enableLight(false);
            lightningBolts.SetActive(true);
            RenderSettings.skybox.SetColor("_Tint", Color.grey);
            RenderSettings.skybox.SetFloat("_Exposure", 0.2F);
        }
        else if (levelType == LevelType.BLOODMOON)
        {
            moonlight.SetActive(true);
            moonlight.GetComponent<MoonlightScript>().enableLight(true);
            lightningBolts.SetActive(false);
            moonlight.GetComponent<MoonlightScript>().redden();
            RenderSettings.skybox.SetColor("_Tint", Color.red);
            RenderSettings.skybox.SetFloat("_Exposure", 0.5F);
        }
        else
        {
            moonlight.SetActive(false);
            moonlight.GetComponent<MoonlightScript>().enableLight(false);
            lightningBolts.SetActive(true);
            lightningBolts.GetComponent<LightningScript>().redden();
            RenderSettings.skybox.SetColor("_Tint", new Color(0.8F, 0.4F, 0.4F));
            RenderSettings.skybox.SetFloat("_Exposure", 0.1F);
        }
    }

    public void clearDecor()
    {
        foreach (GameObject decor in decorObjects) Destroy(decor);
        decorObjects.Clear();

        foreach (GameObject enemy in enemies) Destroy(enemy);
        enemies.Clear();

        moonlight.SetActive(false);
        moonlight.GetComponent<MoonlightScript>().enableLight(false);
        lightningBolts.SetActive(false);
    }

    public void setValues(float radius, float floorPos)
    {
        this.radius = radius;
        this.floorPos = floorPos;
    }

    public int levelUp()
    {
        level++;
        //estStartingEnemies += enemyIncPerLevel;
        estStartingEnemies = startingEnemies + (level * enemyIncPerLevel);
        GetComponent<GenerateFloor>().resetLevel();
        return level;
    }

    public void itemWasTaken()
    {
        itemsCollected++;
    }

    private GameObject getNextItem()
    {
        if (itemsCollected == items.Length) return null;
        int index = Random.Range(0, items.Length - itemsCollected);
        
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i].GetComponent<ItemScript>().isTaken()) continue;
            if (index == 0)
            {
                items[i].SetActive(true);
                return items[i];
            }
            index--;
        }

        Debug.Log("There's been a mistake");
        return null;
    }

    private void assignRoomtypes(Cell[][] cells, int rows, int columns)
    {
        int counter = 0;
        int libraryIndex = Random.Range(0, decoratableCells - 2);
        int armoryIndex = Random.Range(0, decoratableCells - 2);
        int armoryIndex_2;
        int armoryIndex_3;
        if (getLevelType() == LevelType.BLOODMOON)
        {
            armoryIndex_2 = Random.Range(0, decoratableCells - 2);
            armoryIndex_3 = -1;
        }
        else if (getLevelType() == LevelType.HELLISH)
        {
            armoryIndex_2 = Random.Range(0, decoratableCells - 2);
            armoryIndex_3 = Random.Range(0, decoratableCells - 2);

            necromancer.SetActive(true);
        }
        else
        {
            armoryIndex_2 = -1;
            armoryIndex_3 = -1;
        }
        for (int i = 0; i < columns; i++)
        {
            for (int j = 0; j < rows; j++)
            {
                if (cells[i][j].dead || cells[i][j].outskirt) continue;
                if (cells[i][j] == closestCell || cells[i][j] == farthestCell) continue;
                if (counter == armoryIndex) cells[i][j].roomType = RoomType.ARMORY;
                if (counter == armoryIndex_2) cells[i][j].roomType = RoomType.ARMORY;
                if (counter == armoryIndex_3) cells[i][j].roomType = RoomType.ARMORY;
                if (counter == libraryIndex) cells[i][j].roomType = RoomType.LIBRARY;
                counter++;
            }
        }
    }

    private void placeItemCenter(Cell cell, GameObject item)
    {
        if (item == null) return;

        float xPos = cell.xPos - radius;
        float zPos = cell.yPos - radius;

        float yPos = 1.2F;

        item.transform.position = new Vector3(xPos, yPos, zPos);
    }

    private void placeDecorCenter(Cell cell, GameObject decor)
    {
        if (decor == null) return;

        float xPos = cell.xPos - radius;
        float zPos = cell.yPos - radius;

        float yPos = floorPos;

        decor.transform.position = new Vector3(xPos, yPos, zPos);
    }

    private void placeTwo(Cell cell, GameObject thing1, GameObject thing2)
    {
        float xPos = cell.xPos - radius + 1;
        float zPos = cell.yPos - radius;

        float yPos = floorPos;

        thing1.transform.position = new Vector3(xPos, yPos, zPos);

        xPos = cell.xPos - radius - 1;

        thing2.transform.position = new Vector3(xPos, yPos, zPos);
    }

    private void placeThree(Cell cell, GameObject thing1, GameObject thing2, GameObject thing3)
    {
        float xPos = cell.xPos - radius + 1;
        float zPos = cell.yPos - radius + 1;

        float yPos = floorPos;

        thing1.transform.position = new Vector3(xPos, yPos, zPos);

        xPos = cell.xPos - radius - 1;

        thing2.transform.position = new Vector3(xPos, yPos, zPos);

        xPos = cell.xPos - radius;
        zPos = cell.yPos - radius - 1;

        thing3.transform.position = new Vector3(xPos, yPos, zPos);
    }

    private void placeNearCenter(Cell cell, GameObject[] stuff)
    {
        switch (stuff.Length)
        {
            case 0:
                break;
            case 1:
                placeDecorCenter(cell, stuff[0]);
                break;
            case 2:
                placeTwo(cell, stuff[0], stuff[1]);
                break;
            case 3:
                placeThree(cell, stuff[0], stuff[1], stuff[2]);
                break;
            default:
                Debug.Log("There's been some kind of mistake");
                break;
        }
    }

    private GameObject spawnEnemy()
    {
        GameObject enemy;
        float rand = Random.Range(0, 20);
        if (rand == 0)
        {
            enemy = Instantiate(troll, enemyHolder.transform);
            enemy.name = "Troll";
        }
        else if (rand < 5)
        {
            enemy = Instantiate(goblin, enemyHolder.transform);
            enemy.name = "Goblin";
        }
        else
        {
            enemy = Instantiate(zombie, enemyHolder.transform);
            enemy.name = "Zombie";
        }
        enemy.GetComponent<EnemyScript>().level = level;
        enemy.SetActive(true);
        return enemy;
    }

    public enum LevelType
    {
        NORMAL, LIGHTNING, BLOODMOON, HELLISH
    }

    public enum RoomType
    {
        DINING, ARMORY, LIBRARY
    }

    public LevelType getLevelType()
    {
        if (level < lightningLevel) return LevelType.NORMAL;
        else if (level < bloodmoonLevel) return LevelType.LIGHTNING;
        else if (level < hellishLevel) return LevelType.BLOODMOON;
        else return LevelType.HELLISH;
    }
}
