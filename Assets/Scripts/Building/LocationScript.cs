﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationScript : MonoBehaviour {

    public GameObject building;

    private GenerateFloor buildingScript;
    private GameObject room = null;
    private Cell cell = null;

    // Use this for initialization
    void Start ()
    {
        buildingScript = building.GetComponent<GenerateFloor>();
	}

    // Update is called once per frame
    void FixedUpdate()
    {
    }

    public void determineCell(Cell cell1, Cell cell2)
    {
        if (cell1.getMarker() == room) cell = cell1;
        else cell = cell2;
    }

    public Cell getCell() { return cell; }
    public GameObject getRoom() { return room; }
    public bool inLibrary() { return (cell.roomType == DecorManager.RoomType.LIBRARY); }

    private void OnTriggerEnter(Collider other)
    {
        string otherTag = other.gameObject.tag;
        if (otherTag == "Room")
        {
            room = other.gameObject;
            cell = other.gameObject.GetComponent<RoomScript>().getCell();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //if (room == null || cell == null || room != cell.getMarker()) OnTriggerEnter(other);
        OnTriggerEnter(other);
    }
}
