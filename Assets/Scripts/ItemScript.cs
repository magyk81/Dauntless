﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour {

    public GameObject GUI;
    public GameObject building;

    public GameObject text;
    public GameObject background;

    public int degreesPerFrame = 3;
    public bool spin;
    public bool weaponUpgrade;
    public string pickupMessage = "Implying video games are fun";
    public int itemIndex;

    private int rotationAmount = 0;

    private bool taken = false;
    private int countDown = 120;

    private bool victory = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (taken && countDown > 0)
        {
            countDown--;
            if (countDown == 0) gameObject.SetActive(false);
        }

        if (!spin) return;

        rotationAmount += degreesPerFrame;
		if (rotationAmount >= 360)
        {
            rotationAmount = 0;
        }

        transform.localEulerAngles = new Vector3(45, rotationAmount, 0);
    }

    public bool isTaken() { return taken; }

    public void setVictory() { victory = true; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Player" || taken) return;
        if (spin)
        {
            GUI.GetComponent<CanvasManage>().displayMessage(pickupMessage);
            if (weaponUpgrade)
                other.gameObject.GetComponent<PlayerScript>().switchWeapon();
            GetComponent<AudioSource>().Play();
            other.gameObject.GetComponent<CollectionManager>().obtainItem(itemIndex);
            taken = true;
            building.GetComponent<DecorManager>().itemWasTaken();
            transform.localScale = new Vector3(0, 0, 0);
        }
        else if (victory)
        {
            makeVictoryScreen();
            GameObject.Find("GUI Manager").GetComponent<CanvasManage>().death();
        }
        else
        {
            int currentLevel = building.GetComponent<DecorManager>().levelUp();
            GUI.GetComponent<CanvasManage>().displayMessage(pickupMessage + " " + currentLevel);
        }
    }

    private void makeVictoryScreen()
    {
        text.GetComponent<UnityEngine.UI.Text>().text = "You win!";
        text.GetComponent<UnityEngine.UI.Text>().color = new Color(0, 0, 0);
        background.GetComponent<UnityEngine.UI.RawImage>().color = new Color(0.9F, 0.9F, 0.9F);
    }
}
