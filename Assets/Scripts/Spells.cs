﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spells
{
    private string description;
    private string hotKey;
    private int spellRank;
    private int cost;

    private int spellType;
    
    public Spells(int t, string hKey, int r, string desc)
    {
        description = desc;
        hotKey = hKey;
        spellRank = r;
        spellType = t;
        findCost(t, r);
    }
    public void updateRank()
    {
        spellRank++;
        findCost(spellType, spellRank);
        Debug.Log("\n" + spellRank);
    }
    public string getDescription() { return description; }
    public string getHotKey() { return hotKey; }
    public int getRank() { return spellRank; }
    public int getCost() { findCost(spellType, spellRank); return cost; }
    private void findCost(int type, int rank)
    {
        if (type == 0)
        {
            if (rank == 0)
            {
                cost = 0;
            }
            else if (rank == 1)
            {
                cost = 5;
            }
            else if (rank == 2)
            {
                cost = 20;
            }
        }
        else if (type == 1)
        {
            if (rank == 0)
            {
                cost = 50;
            }
            else if (rank == 1)
            {
                cost = 15;
            }
            else if (rank == 2)
            {
                cost = 45;
            }
        }
        else if (type == 2)
        {
            if (rank == 0)
            {
                cost = 20;
            }
            else if (rank == 1)
            {
                cost = 4;
            }
            else if (rank == 2)
            {
                cost = 8;
            }
        }
        else if (type == 3)
        {
            if (rank == 0)
            {
                cost = 10;
            }
            else if (rank == 1)
            {
                cost = 5;
            }
            else if (rank == 2)
            {
                cost = 25;
            }
        }
        else cost = 0;
    }
}
