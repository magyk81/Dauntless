# Final Project - Dauntless

<h4>CS 491 / IFDM 491 - Spring 2018</h4>

<br><b>About:</b><br>
  <br>Zombie horde survival game having themes of horror and sword-and-sorcery.
  <br>Uses elements of fantasy RPG and melee.<br>
  <br>The game's objective is to reach the goal at the end of each level, which
  <br>is indicated by a yellow ladder.
  <br>There are 4 levels total. Each level has one library and one armory.
  <br>An armory usually has a collectible item that improves the player's armor
  <br>rating or attack power. Standing in a library allows the player to access
  <br>a menu to purchase magic spells using points that were earned by killing
  <br>enemies. Each level has more numerious and aggressive enemies than the last.<br>
  <br>There are three main enemy types:
  
+ <b>Zombie:</b> Relatively easy to deal with, but come in large numbers
+ <b>Goblin:</b> Easier to kill and do less damage, but move quicker
+ <b>Troll:</b> Much harder to kill, do more damage, and only flinch against<br>
a stronger sword

  The final boss is the <i>Necromancer</i>. He only appears in the final level and
  <br>cannot be killed. He moves through walls and other obstacles, and drains
  <br>the player's health when in contact with him/her. Using the <i>Light</i> spell
  <br>causes the necromancer to flee while the spell is active.
  
![alt text][Unbreathing]
[Unbreathing]: https://magic.wizards.com/sites/mtg/files/image_legacy_migration/images/magic/daily/sf/sf184_horde.jpg '"Unbreathing Horde" by Dave Kendall'

<br><b>Team Members:</b>

  + Aliyah Reese
  + Robin Campos
  + Dustin Loughrin
  + Noah Tijerina<br>

<br><b>Controls:</b><br>
  <br><kbd>A</kbd> <i>move left</i>
  <br><kbd>D</kbd> <i>move right</i>
  <br><kbd>W</kbd> <i>move forward</i>
  <br><kbd>S</kbd> <i>move backward</i>
  <br><kbd>MOUSE MOVE</kbd> <i>turn</i>
  <br><kbd>LEFT CLICK</kbd> <i>attack</i>
  <br><kbd>RIGHT CLICK</kbd> <i>(hold down) cast selected spell</i>
  <br><kbd>SHIFT</kbd> <i>(hold down) haste spell</i>
  <br><kbd>MOUSE SCROLL</kbd> <i>select spell</i>
  
<br><b>Credits:</b><br>
  <br>Zombie texture from "Necrosis Painting" by Eric Gaston Simard
  <br>Font in menus: "Supernatural Knight" by Matthew Walters (dafont.com)
  <br>Font for library menu: "Scurlock" by The Scriptorium (dafont.com)
  <br>Lightning: "Simple Lightning Bolt" by Jeff Johnson (Unity Asset Store)
  <br>Game Sounds from freesound.org: 
*     Picking up items: "Pickup.wav" by morganpurkis
*     Hitting enemies: "Big Zombie Hit 1" by Slave2theLight
*     Being hit by zombie/goblin: "Smashing head on wall" by pfranzen
*     Being hit by troll "Hit 1" by ihitokage
  <br>

![alt text][Damned]
[Damned]: http://s3.gatheringmagic.com/uploads/2013/04/02/AW_1.jpg '"Army of the Damned" by Ryan Pancoast'

### Deadlines
---

| Date     | Thing                           |
|:-------- |:------------------------------- |
| March 18 | Game Design Document due        |
| April 3  | Alpha Playtest                  |
| April 8  | Report due                      |
| April 24 | Beta Playtest                   |
| April 29 | Report due                      |
| May 3    | Individual/Team Evaluations due |
| May 6    | Final Game due                  |
| May 12   | Final Game demo                 |

![alt text][Ranks]
[Ranks]: https://dfep0xlbws1ys.cloudfront.net/thumbs39/ae/39aeb4cfbcd9bc28cc7bdece49dd14be.jpg '"Endless Ranks of the Dead" by Ryan Yee'